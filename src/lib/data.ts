export const statsObjects = [
  {
    hits: 100,
    misses: 30,
    chillies: {
      jalapeno: 2,
      birdsEye: 4,
    },
  },
  {
    hits: 20,
    misses: 10,
    jumps: 5,
    chillies: {
      jalapeno: 8,
      birdsEye: 7,
    },
  },
];

export const books = [
  {
    id: "1",
    title: "The Awakening",
    authorId: "1",
  },
  {
    id: "2",
    title: "City of Glass",
    authorId: "2",
  },
  {
    id: "3",
    title: "City of Glass 2",
    authorId: "2",
  },
];

export const authors = [
  {
    id: "1",
    name: "Kate Chopin",
  },
  {
    id: "2",
    name: "Paul Auster",
  },
];
