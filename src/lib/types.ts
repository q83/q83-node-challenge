export type Author = {
  id: string;
  name: string;
};

export type Book = {
  id: string;
  title: string;
  authorId: string;
};

export type OperationContext = {
  requestCount: number;
};
