import { ApolloServer, gql } from "apollo-server-express";
import { Book, Author, OperationContext } from "./lib/types";
import { getAuthorsForIds, getBooksForIds, getBooksForAuthorIds } from "./db";

const typeDefs = gql`
  type Book {
    title: String
    author: Author
  }

  type Author {
    name: String
    books: [Book]
  }

  type Query {
    listBooks: [Book]
    listAuthors: [Author]
  }
`;

const resolvers = {
  Query: {
    listBooks: (parent: null, args: null, context: OperationContext) =>
      getBooksForIds(null, context),
    listAuthors: (parent: null, args: null, context: OperationContext) =>
      getAuthorsForIds(null, context),
  },
  Author: {
    books: (parent: Author, args: null, context: OperationContext) => {
      return getBooksForAuthorIds([parent.id], context);
    },
  },
  Book: {
    author: (parent: Book, args: null, context: OperationContext) => {
      const authors = getAuthorsForIds([parent.authorId], context);
      return authors[0];
    },
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  playground: true,
  context: {
    requestCount: 0,
  },
  plugins: [
    {
      requestDidStart() {
        return {
          willSendResponse(requestContext) {
            console.log("Request count: ", requestContext.context.requestCount);
          },
        };
      },
    },
  ],
});

export default server;
