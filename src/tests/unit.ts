import { describe, it } from "mocha";
import { isEqual } from "lodash";
import { assert, expect } from "chai";
import { getAverageOfObjects, recursivelyExecutePromises } from "../app";
import { statsObjects } from "../lib/data";

describe("getAverageOfObjects", () => {
  it("should average n objects", (done) => {
    const result = getAverageOfObjects(statsObjects);
    assert(
      isEqual(result, {
        hits: 60,
        misses: 20,
        jumps: 5,
        chillies: {
          jalapeno: 5,
          birdsEye: 5.5,
        },
      })
    );
    done();
  });
});

describe("recursivelyExecutePromises", () => {
  it("should run promises synchronously", async () => {
    const resultsInOrder: any[] = [];

    const asynchronousFunction = (value: string, timeout: number) => () =>
      new Promise((resolve) => {
        const delay = Math.round(timeout || Math.random() * 1000);

        setTimeout(() => {
          const result = `${value}-${delay}`;
          resultsInOrder.push(result);
          resolve(result);
        }, delay);
      });

    const promises = [
      asynchronousFunction("1", 10),
      asynchronousFunction("2", 4),
      asynchronousFunction("3", 6),
    ];

    return recursivelyExecutePromises(promises).then((results) => {
      expect(resultsInOrder).to.deep.equal(results);
    });
  });
});
