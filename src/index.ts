import express from "express";
import graph from "./graph";
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

graph.applyMiddleware({ app, path: "/graphql" });

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
