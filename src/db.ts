import { authors, books } from "./lib/data";
import { OperationContext } from "./lib/types";
import filter from "lodash/filter";

export const getAuthorsForIds = (
  ids: string[] | null,
  context: OperationContext
) => {
  context.requestCount = context.requestCount + 1;
  return filter(authors, (o) => (ids ? ids.includes(o.id) : true));
};

export const getBooksForIds = (
  ids: string[] | null,
  context: OperationContext
) => {
  context.requestCount = context.requestCount + 1;
  return filter(books, (o) => (ids ? ids.includes(o.id) : true));
};

export const getBooksForAuthorIds = (
  ids: string[] | null,
  context: OperationContext
) => {
  context.requestCount = context.requestCount + 1;
  filter(books, (o) => (ids ? ids.includes(o.authorId) : true));
};
