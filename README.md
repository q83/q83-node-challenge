**Challenge Overview**

To make your submission, please take the following steps:

1. Create a branch new branch from this repository
2. Make your changes on your new branch
3. Submit a pull request for your branch

Some ground rules for completing this challenge:

- Take a few minutes to read through these instructiosn but after that, don't spend more than an hour writing your solutions for this challenge.

_Your submission will be judged primarily on your approach to solving problems as opposed to how much you can get through in a small amount of time_

- Use any third-party libraries and internet resources you like.

- You can do the challenges in any order you like.

- Don't be concerned if the tests don't pass, you'll be assessed on your approach.

---

**Installation**

Install dependencies:

```
npm i

```

You may need to install mocha globally to ensure the tests run:

```
npm i -g mocha
```

---

**Challenge 1 - Routing**

In the `src/index.ts` file you must create a new route which will return the results of an equation purely based on the url.

- Use a single route only.
- The equation must accept a variable number of operands _(i.e: 1+2+3, 1+2, 1+2+3+4+5, etc)_
- At a minimum, try to support at least 2 types of equations _(i.e: addition, subtraction, multiplication)_

---

**Challenge 2 - GraphQL**

The `src/graph.ts` file contains a GraphQL server which uses a mock database (`src/db.ts`). The playground can be accessed at http://localhost:3000/graphql after running the node project.

Consider the following query:

```
query {
  listBooks {
    title
    author {
      name
      books {
        title
      }
    }
  }
}
```

It contains nested fields which results in the same data being retrieved from the database multiple times. (In the above case there are 7 database requests).

The requirement of this challenge is to implement a solution that reduces the number of requests required to execute queries such as the above. This can be done through batching, memoization, etc.

_NOTE: You can see the number of database requests logged to the console ("Request count: #")_

---

**Challenge 3 - Recursive Averaging**

In the `src/app.ts` file you must complete the function `getAverageOfObjects` to accept an array of objects of differing shapes and get an average of their values.

You can test your function with the unit tests in `src/tests/unit.ts` by running:

`npm run test`

Some more clarifications:

- The resulting object should be the combined shape of the objects in the function argument. _eg: for these objects..._

`[{a:1, b:2}, {a:3, b:4, c:5}]`

_...the result would be..._

`{a:2, b:3, c:5}`

- When getting the average of a value, divide the total by the number of times that value is found as opposed to the total number of objects. _In the above example, c is divided by 1, not 2 because it only appears once_

---

**Challenge 4 - Promise Handling**

As with the above challenge, your code will go in the `src/app.ts` file. Complete the `recursivelyExecutePromises` function to accept an array of promises and resolve them in the order they appear in the array. _(Normally promises are executed asynchronously and simultaneously)_

- Use the unit tests in `src/tests/unit.ts` to check that the ordered results match the results returned from the function.
- The name of the function is "`recursivelyExecutePromises`" but that doesn't mean it's mandatory for you to use recursion in your solution - it's just a hint!
